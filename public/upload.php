<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Upload File</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
  <style>
    .upload-container {
      margin: 50px auto;
      width: 500px;
    }
  </style>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-12 upload-container">

<?php

// Check if upload form is submitted
if (isset($_POST['submit'])) {
  // Get file information
  $fileName = $_FILES['file']['name'];
  $fileTmpName = $_FILES['file']['tmp_name'];
  $fileSize = $_FILES['file']['size'];
  $fileError = $_FILES['file']['error'];
  $fileType = $_FILES['file']['type'];

  // Check for upload errors
  if ($fileError === 0) {
    // Check if file size is within the allowed limit
    if ($fileSize <= 500000) {
      // Allowed file extensions
      $allowedExtensions = array('jpg', 'jpeg', 'png', 'pdf', 'docx');

      // Get file extension
      $fileExtension = explode('.', $fileName)[1];

      // Check if file extension is allowed
      if (in_array($fileExtension, $allowedExtensions)) {
        // Generate a unique filename to avoid conflicts
        $newFileName = uniqid('', true) . '.' . $fileExtension;

        // Upload the file to the server
        $uploadPath = 'uploads/' . $newFileName;
        move_uploaded_file($fileTmpName, $uploadPath);

        // Database connection and upload information
        // ... Implement your database connection and upload logic here ...

        // Success message
        echo "<p class='alert alert-success'>File uploaded successfully!</p>";
      } else {
        // Error: Invalid file extension
        echo "<p class='alert alert-danger'>Invalid file extension. Please upload only allowed file formats.</p>";
      }
    } else {
      // Error: File size exceeds the limit
      echo "<p class='alert alert-danger'>File size exceeds the limit of 500KB. Please upload a smaller file.</p>";
    }
  } else {
    // Error: File upload failed
    echo "<p class='alert alert-danger'>Error uploading file. Please try again.</p>";
  }
}

?>

<h1>Upload File</h1>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
  <div class="mb-3">
    <label for="file" class="form-label">Select File:</label>
    <input type="file" name="file" id="file" class="form-control" required>
  </div>
  <button type="submit" name="submit" class="btn btn-primary">Upload</button>
</form>

      </div>
    </div>
  </div>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
